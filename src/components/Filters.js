import React, { Component } from 'react';
import '../stylesheets/main.scss';

class Filters extends Component {

  timeout = [];
  // at beginning of click, loop through and clear all other timeouts
  // this.timeouts.push(
  //   setTimeout(() => {
      //call for filtering
  //   }, 400)
  // )

  // Function to handle dietary restrictions dropdown
  handleCheckboxClick = () => {
    const restrictionsEl = document.querySelector('#filter-restrictions');
    const key = "restrictions";
    let stringArr = [];
    let glutenFree = false;

    document.querySelectorAll('input[type="checkbox"]').forEach(function(single) {
      if(single.checked) {
        if (single.value === 'Gluten Free') {
          glutenFree = !glutenFree;
        } else {
          stringArr.push(' ' + single.value)
        }
      }
    })

    if(stringArr.toString() === "") {
      this.props.updateFilters(key, null)
      restrictionsEl.style.fontStyle = 'italic';
    } else {
      this.props.updateFilters(key, stringArr.toString())
      restrictionsEl.style.fontStyle = 'normal';
      restrictionsEl.style.textTransform = 'capitalize';
    }

    this.props.updateGlutenFree(glutenFree)
  }

  // Function to handle resetting filters button press
  resetFilters = () => {
    const resettedFilters = {
      typeOfFood: null,
      restrictions: null,
      sortBy: null
    }

    // Reset checkboxes to unchecked
    document.querySelectorAll('.checkbox').forEach(checkbox => {
      checkbox.checked = false;
    })

    // Reset font style back to italic
    document.querySelector('#filter-restrictions').style.fontStyle = 'italic';
    document.querySelector('#filter-type').style.fontStyle = 'italic';
    document.querySelector('#sort-by-parent').style.fontStyle = 'italic';

    // Reset dropdowns to closed
    document.querySelector('#food-type').classList.remove('display-dropdown');
    document.querySelector('#sort-by').classList.remove('display-dropdown');
    document.querySelector('#restrictions').classList.remove('display-dropdown');

    this.props.resetFilters(resettedFilters)
  }

  // Function to handle type of food and sort by dropdowns
  setFilter = (text) => {
    let key = 'typeOfFood';
    
    if (text === 'Salad' || text === 'Fish' || text === 'Pasta' || text ==='Dessert') {
      
      key = 'typeOfFood';
      document.querySelector('#filter-type').style.fontStyle = 'normal';
      document.querySelector('#food-type').classList.remove('display-dropdown');

    } else if (text === 'Shortest to Longest Time' || text === 'Longest to Shortest Time' || text === 'Lowest to Highest Health Score' || text === 'Highest to Lowest Health Score') {

      key = 'sortBy'
      document.querySelector('#sort-by-parent').style.fontStyle = 'normal';
      document.querySelector('#sort-by').classList.remove('display-dropdown');
    }

    this.props.updateFilters(key, text)
  };

  // Function to handle when to toggle a dropdown and which one to toggle
  toggleDropdown = (e) => {
    const parentEl = document.querySelector(`#${e.target.id}`);
    const parentId = parentEl.id;

    const foodTypeEl = document.querySelector('#food-type');
    const restrictionsEl = document.querySelector('#restrictions');
    const sortingEl = document.querySelector('#sort-by');

    if (parentId === 'filter-type') {

      if (!foodTypeEl.classList.contains('display-dropdown')) {
        foodTypeEl.classList.add('display-dropdown');
      } else {
        foodTypeEl.classList.remove('display-dropdown');
      }
      restrictionsEl.classList.remove('display-dropdown');
      sortingEl.classList.remove('display-dropdown');

    } else if (parentId === 'filter-restrictions') {
      
      if (!restrictionsEl.classList.contains('display-dropdown')) {
        restrictionsEl.classList.add('display-dropdown');
      } else {
        restrictionsEl.classList.remove('display-dropdown');
      }
      foodTypeEl.classList.remove('display-dropdown');
      sortingEl.classList.remove('display-dropdown');
      
    } else {

      if (!sortingEl.classList.contains('display-dropdown')) {
        sortingEl.classList.add('display-dropdown');
      } else {
        sortingEl.classList.remove('display-dropdown');
      }
      foodTypeEl.classList.remove('display-dropdown');
      restrictionsEl.classList.remove('display-dropdown');
    }
  };

  render() {
    // Initialize filter arrays
    let typeFilters = [];
    let restrictionsFilters = [];
    let sortFilters = [];

    // Loop to prepare the type of food array
    for (let i = 0; i < this.props.typeArray.length; i++) {
      typeFilters.push(
        <li key={i} onClick={this.setFilter.bind(null, this.props.typeArray[i])}> 
          {this.props.typeArray[i]} 
        </li>
      );
    }

    // Loop to prepare the restrictions array
    for (let i = 0; i < this.props.restrictionsArray.length; i++) {
      restrictionsFilters.push(
        <li key={i}>
          <input onChange={this.handleCheckboxClick.bind(null,this.props.restrictionsArray[i])} id={i} type="checkbox" className="checkbox" value={this.props.restrictionsArray[i]}/>
          <label  htmlFor={i} className="checkbox-label">
            {this.props.restrictionsArray[i]} 
          </label>
        </li>
      );
    }

    // Loop to prepare the sort by array
    for (let i = 0; i < this.props.sortArray.length; i++) {
      sortFilters.push(
        <li key={i} onClick={this.setFilter.bind(null, this.props.sortArray[i])}> 
          {this.props.sortArray[i]} 
        </li>
      );
    }

    return(
      <div className="container">

      {/* Type of Food Code Block */}
        <div className="dropdown">
          <p id="filter-type" onClick={this.toggleDropdown} className="dropdown--heading">
          {(this.props.filters.typeOfFood != null) ? this.props.filters.typeOfFood : 'Filter Type of Food...'}
          </p>

          <ul id="food-type">
            {typeFilters}
          </ul>
        </div>

      {/* Dietary Restrictions Code Block */}
        <div className="dropdown">
          <p id="filter-restrictions" onClick={this.toggleDropdown} className="dropdown--heading">
            {(this.props.filters.restrictions != null) ? this.props.filters.restrictions : 'Filter Dietary Restrictions...'} 
          </p>

          <ul id="restrictions">
            {restrictionsFilters}
          </ul>
        </div>

      {/* Sort By Code Block */}
        <div className="dropdown">
          <p id="sort-by-parent" onClick={this.toggleDropdown} className="dropdown--heading">
            {(this.props.filters.sortBy != null) ? this.props.filters.sortBy : 'Sort By...'} 
          </p>

          <ul id="sort-by">
            {sortFilters}
          </ul>
        </div>

        <button onClick={this.resetFilters} className="button--reset">
          Reset All
        </button>
      </div>
    )
  }
}

export default Filters;