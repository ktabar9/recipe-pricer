import React from 'react';
import '../stylesheets/main.scss';

const Grid = (props) => {
    const imgUrl = 'https://webknox.com/recipeImages/' + props.recipe.image; 
    var recipeStyle = {
      backgroundImage: 'url(' + imgUrl + ')',
    }

    return(
      <div onClick={props.onClick} className="container--tile" style={recipeStyle}>

        <div className="tile">
          <h3 className="tile--title">{props.recipe.title}</h3>
        </div>

        <div className="tile--bottom">
          <p className="tile--info">
            Ready-in: 
            <span className="tile--value">{props.recipe.readyInMinutes} min</span> 
          </p>
          <p className="tile--info">
            HealthScore: <span className="tile--value">{props.recipe.healthScore ? props.recipe.healthScore : '--'}</span>
          </p>
        </div>

      </div>
    )
}

export default Grid;