import React from 'react';
import '../stylesheets/main.scss';

const Instructions = (props) => {
  const instructionStep = props.instructions.map(singleInstruction => {
    return (
      <li key={singleInstruction}>
        <span>{singleInstruction}</span>
      </li>
    )
  })

  let ingredients = [];

  // Loop to prepare ingredient list and prices
  for (let i = 0; i < props.ingredientList.length; i++) {
    ingredients.push(
      <p key={i} className="ingredient-list--sub">
        {props.ingredientList[i]}
      </p>
    );
  };

  return(
    <div>
      <h2 className="card--title">Recipe Instructions</h2>
      <div className="card--instructions">
        <h3 className="card--subtitle">
          Ingredients:
        </h3>

        <div className="ingredient-list">
          {ingredients}
        </div>

        <h3 className="card--subtitle">
          Steps:
        </h3>

        {instructionStep}
      </div>
    </div>
  )
}

export default Instructions;