import React from "react";

const Header = (props) => (
  <header className="header">
    <p className="header--title">Recipe Pricer</p>
    <p className="header--button" onClick={props.onClick}>
      <span>{props.currentPath === '/' ? '' : 'Back'}</span>
    </p>
  </header>
);

export default Header;