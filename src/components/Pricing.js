import React from 'react';
import '../stylesheets/main.scss';

const Pricing = (props) => {

  let ingredients = [];

  // Loop to prepare ingredient list and prices
  for (let i = 0; i < props.ingredients.length; i++) {
    ingredients.push(
      <div key={i}>
        <p className="ingredient--left">
          {props.ingredients[i].name}
        </p>
        <p className="ingredient--right">
          ${props.ingredients[i].usableCost.toFixed(2)}
        </p>
      </div>
    );
  };

  return(
    <div>
      <h2 className="card--title">
        Pricing Information
      </h2>

      <div id="ingredient-list" className="ingredient">
        {ingredients}
      </div>

      <div className="total">
        <p>
          Total:
          <span className="total--price">${props.totalCost.toFixed(2)}</span>
        </p>
      </div>

      <button onClick={props.onClick} className="button button--pricing">
        <span id="button-text">View </span>
        Breakdown
      </button>
    </div>
  )
}

export default Pricing;