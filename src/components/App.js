import React, { Component } from 'react';
import '../stylesheets/main.scss';
import Header from './Header';
import Grid from './Grid';
import Filters from './Filters';
import { getRecipeList, getSingleRecipeData } from '../APICalls/GetRecipes';

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      path: '',
      filters: {
        typeOfFood: null,
        restrictions: null,
        sortBy: null
      },
      glutenFree: false,
      loading: true,
      recipes: [],
      currentRecipe: {}
    }
  }

  componentDidMount = () => {
    const location = this.props.location.pathname;
    this.setState({path: location});

    this.getRecipeEverything();
  }

  componentDidUpdate = (prevProps, prevState) => {
    if(this.state.filters !== prevState.filters) {
      this.setState({loading: true});
      this.getRecipeEverything();
    }
  }


  getRecipeEverything = async () => {
    let recipes = await getRecipeList(
      this.state.filters['typeOfFood'],
      this.state.filters['restrictions'],
      this.state.glutenFree
    );

    recipes = recipes.results;
    
    for (let i=0; i < recipes.length; i++) {
      await getSingleRecipeData(recipes[i].id).then(
        result => {
          recipes[i].healthScore = result.status && result.status === 'failure' ? '--' : result.healthScore;
        }
      );
    }

    const sortedRecipes = this.sortRecipes(this.state.filters.sortBy, recipes);
    
    this.setState({ recipes: sortedRecipes, loading: false });
  }

  navigateToRecipe = (recipe) => {
    const location = `/recipe/${recipe.id}/${recipe.title}`;
    this.props.history.push(`/recipe/${recipe.id}/${recipe.title}`);

    this.setState({path: location});
    this.setState({currentRecipe: recipe})
  }

  resetFilters = (resettedFilters) => {
    this.setState({filters: resettedFilters})
    this.setState({glutenFree: false})
  }

  sortRecipes(sortBy, recipes) {

    if (!sortBy) {
      return recipes;
    }

    if(sortBy.toLowerCase() === 'shortest to longest time') {
      recipes.sort(function(a, b) {
        if (a.readyInMinutes < b.readyInMinutes) {
          return -1
        } else if (a.readyInMinutes > b.readyInMinutes) {
          return 1
        } else {
          return 0
        }
      })
    } else if(sortBy.toLowerCase() === 'longest to shortest time'){
      recipes.sort(function(a, b) {
        if (a.readyInMinutes > b.readyInMinutes) {
          return -1
        } else if (a.readyInMinutes < b.readyInMinutes) {
          return 1
        } else {
          return 0
        }
      })
    } else if(sortBy.toLowerCase() === 'lowest to highest health score'){
      recipes.sort(function(a, b) {
        if (a.healthScore < b.healthScore) {
          return -1
        } else if (a.healthScore > b.healthScore) {
          return 1
        } else {
          return 0
        }
      })
    } else if(sortBy.toLowerCase() === 'highest to lowest health score'){
      recipes.sort(function(a, b) {
        if (a.healthScore > b.healthScore) {
          return -1
        } else if (a.healthScore < b.healthScore) {
          return 1
        } else {
          return 0
        }
      })
    }
    
    
    return recipes;
  }

  updateGlutenFree = (glutenFree) => {
    this.setState({glutenFree: glutenFree})
  }

  updateFilters = (key, filter) => {
    const filters = {...this.state.filters}
    filters[key] = filter
    this.setState({filters})
  }

  render() {
    const loading = this.state.loading;

    const restrictionsArray = [
      'Gluten Free',
      'Vegetarian',
      'Pescetarian',
      'Vegan'
    ];
    const sortArray = [
      'Shortest to Longest Time',
      'Longest to Shortest Time',
      'Lowest to Highest Health Score',
      'Highest to Lowest Health Score'
    ];
    const typeArray = [
      'Salad', 
      'Fish', 
      'Pasta', 
      'Dessert'
    ];
    
    if(loading) {
      return (
        <div className="app">
          <Header 
            currentPath={this.state.path} 
          />
  
          <Filters 
            filters={this.state.filters} //state of filters
            resetFilters={this.resetFilters} //function for resetting
            restrictionsArray={restrictionsArray} //restrictions array
            sortArray={sortArray} //sort array
            typeArray={typeArray} //type array
            updateFilters={this.updateFilters} //function for updating filters
            updateGlutenFree={this.updateGlutenFree} //function for handling gluten free
          />

          <div className="loader">
            Loading...
          </div>
          
          {/* Loader taken from  http://tobiasahlin.com/spinkit/ */}
          <div className="sk-circle"> 
            <div className="sk-circle1 sk-child"></div>
            <div className="sk-circle2 sk-child"></div>
            <div className="sk-circle3 sk-child"></div>
            <div className="sk-circle4 sk-child"></div>
            <div className="sk-circle5 sk-child"></div>
            <div className="sk-circle6 sk-child"></div>
            <div className="sk-circle7 sk-child"></div>
            <div className="sk-circle8 sk-child"></div>
            <div className="sk-circle9 sk-child"></div>
            <div className="sk-circle10 sk-child"></div>
            <div className="sk-circle11 sk-child"></div>
            <div className="sk-circle12 sk-child"></div>
          </div>
        </div>
      )
    }

    if(!loading && (this.state.recipes.length === 0)) {
      return (
        <div className="app">
          <Header 
            currentPath={this.state.path} 
          />
  
          <Filters 
            filters={this.state.filters} //state of filters
            resetFilters={this.resetFilters} //function for resetting
            restrictionsArray={restrictionsArray} //restrictions array
            sortArray={sortArray} //sort array
            typeArray={typeArray} //type array
            updateFilters={this.updateFilters} //function for updating filters
            updateGlutenFree={this.updateGlutenFree} //function for handling gluten free
          />

          <p className="no-results">
            There are no results for your query.
          </p>
        </div>
      )
    }

    return (
      <div className="app">
        <Header 
          currentPath={this.state.path} 
        />

        <Filters 
          filters={this.state.filters} //state of filters
          resetFilters={this.resetFilters} //function for resetting
          restrictionsArray={restrictionsArray} //restrictions array
          sortArray={sortArray} //sort array
          typeArray={typeArray} //type array
          updateFilters={this.updateFilters} //function for updating filters
          updateGlutenFree={this.updateGlutenFree} //function for handling gluten free
        />

        <div className="container container--grid">
          {this.state.recipes.map(recipe =>  // map though all recipes to display grids
            { if(recipe.healthScore) {  // only show recipes that have more information
              return (<Grid 
                key={recipe.id} 
                onClick={() => this.navigateToRecipe(recipe)} 
                recipe={recipe}
                />)
              } else {
                return ''
              }
            }
          )}
        </div>
      </div>
    );
  }
}

export default App;
