import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import App from './App';
import RecipeMain from './RecipeMain';

const Router = () => (
  <BrowserRouter>
    <Switch>
      <Route exact path="/" component={App}/>
      <Route exact path="/recipe/:recipeId/:recipeTitle" component={RecipeMain} />
    </Switch>
  </BrowserRouter>
);

export default Router;