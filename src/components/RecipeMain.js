import React, { Component }  from 'react';
import '../stylesheets/main.scss';
import Header from './Header';
import Instructions from './Instructions';
import Pricing from './Pricing';
import { getSingleRecipeData, getSingleIngredient } from '../APICalls/GetRecipes';

class RecipeMain extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: true,
      currentRecipe: {}
    }
  }

  // Variables from props
  recipeTitle = this.props.match.params.recipeTitle;
  recipeId = this.props.match.params.recipeId;

  // Component variables
  ingredient = {};
  numOfIngredients = 0;
  ingredientsWithAmounts = [];
  ingredientsWithPrices = [];
  instructions = [];
  newHeight = '10px'; // Calculate height by number of ingredients
  recipe = {};
  totalCost = 0;

  componentDidMount = () => {
    this.getRecipeEverything();
  }

  getRecipeEverything = async () => {
    this.setState({ loading: true })
    this.ingredientsWithAmounts = [];
    this.ingredientsWithPrices = [];
    this.totalCost = [];
    this.instructions = [];

    const recipe = await getSingleRecipeData(this.recipeId); 
    const listOfIngredients = recipe['extendedIngredients']

    for (let i = 0; i < listOfIngredients.length; i++) {
      const ingredient = await getSingleIngredient(listOfIngredients[i].name, listOfIngredients[i].amount, listOfIngredients[i].unit); 
      
      this.getUsablePrice(ingredient[0])
    }

    this.setState({ currentRecipe: recipe })
    
    // Set Component Variables
    this.numOfIngredients = recipe['extendedIngredients'].length;
    this.newHeight = this.numOfIngredients * 40;

    for (let i = 0; i < recipe['analyzedInstructions'][0]['steps'].length; i++) {
      this.instructions.push(recipe['analyzedInstructions'][0]['steps'][i]['step'])
    }

    for (let i = 0; i < recipe['extendedIngredients'].length; i++) {
    this.ingredientsWithAmounts.push(recipe['extendedIngredients'][i]['original'])
    }

    this.setState({ loading: false })
  };


  // call function to change estimatedCost into something usable

  getUsablePrice = (ingredient) => {
    let newIngredient = ingredient;

    newIngredient.usableCost = (newIngredient.estimatedCost.value / 100);
    this.ingredientsWithPrices.push(newIngredient)

    this.calculateTotalPrice(newIngredient.usableCost)
  }
  
  calculateTotalPrice = (newCost) => {
    this.totalCost = Number(this.totalCost) + Number(newCost);
  }

  navigateHome = () => {
    this.props.history.push(`/`);
  }

  expandTotal = () => {
    const parentEl = document.querySelector('#ingredient-list');
    const buttonText = document.querySelector('#button-text')

    if (buttonText.innerHTML.trim() === 'View') {
      parentEl.classList.add('ingredient--display');
      parentEl.style.height = this.newHeight + 'px';
      buttonText.innerHTML = 'Close '
    } else {
      parentEl.classList.remove('ingredient--display')
      parentEl.style.height = 10 + 'px';
      buttonText.innerHTML = 'View '
    }      
  }

  render() {
    if (this.state.loading) {
      return (
        <div>
          <Header 
            currentPath={this.props.location.pathname} 
            onClick={() => this.navigateHome()}
          />

          <h2 className="page-title">
            {this.recipeTitle}
          </h2>

          <div className="loader">
            Loading...
          </div>
          
          {/* Loader taken from  http://tobiasahlin.com/spinkit/ */}
          <div className="sk-circle"> 
            <div className="sk-circle1 sk-child"></div>
            <div className="sk-circle2 sk-child"></div>
            <div className="sk-circle3 sk-child"></div>
            <div className="sk-circle4 sk-child"></div>
            <div className="sk-circle5 sk-child"></div>
            <div className="sk-circle6 sk-child"></div>
            <div className="sk-circle7 sk-child"></div>
            <div className="sk-circle8 sk-child"></div>
            <div className="sk-circle9 sk-child"></div>
            <div className="sk-circle10 sk-child"></div>
            <div className="sk-circle11 sk-child"></div>
            <div className="sk-circle12 sk-child"></div>
          </div>
        </div>
      )
    }

    return(
      <div>
        <Header 
          currentPath={this.props.location.pathname} 
          onClick={() => this.navigateHome()}
        />
  
        <h2 className="page-title">
          {this.recipeTitle}
        </h2>

        <div className="container--information">
          <Pricing 
            onClick={() => this.expandTotal()} 
            ingredients={this.ingredientsWithPrices}
            numOfIngredients={this.numOfIngredients}
            totalCost={this.totalCost}
          />
        </div>

        <div className="container--information">
          <Instructions 
            instructions={this.instructions} 
            ingredientList={this.ingredientsWithAmounts}
          />
        </div>
      </div>
    )
  }
}

export default RecipeMain;