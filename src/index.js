import React from 'react';
import { render } from 'react-dom';
import './stylesheets/main.scss';
import Router from './components/Router';

// render(<StorePicker />, document.querySelector('#main'));
render(<Router />, document.querySelector('#main'));

// ReactDOM.render(<App />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
// serviceWorker.unregister();
