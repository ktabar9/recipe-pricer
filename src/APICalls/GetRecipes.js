export function getRecipeList(typeOfFood, restrictions, glutenFree) {
  let queryString = '';
  let restrictionString = '';
  let glutenString = ''

  if(typeOfFood !== null) {
    queryString += `&query=${typeOfFood.toLowerCase()}` 
  }
  if(restrictions !== null) {
    restrictionString += `&diet=${restrictions.trim().toLowerCase()}` 
  }
  if(glutenFree) {
    glutenString += `&intolerances=gluten`
  }

  const response = fetch("https://spoonacular-recipe-food-nutrition-v1.p.rapidapi.com/recipes/search?number=20" + queryString + restrictionString + glutenString, {
    method: 'GET',
    headers: new Headers({
      'X-RapidAPI-Key': '55d98e3beemshee3584b0a12de42p1cd3c6jsn38172b345589'
    }),
  }).then(response => response.json())

  return response;
}

export function getSingleRecipeData(id) {
  let response = fetch("https://spoonacular-recipe-food-nutrition-v1.p.rapidapi.com/recipes/" +  id + "/information", {
    method: 'GET',
    headers: new Headers({
      'X-RapidAPI-Key': '55d98e3beemshee3584b0a12de42p1cd3c6jsn38172b345589'
    })
  }).then(response => response.status !== '404' ? response.json() : '')
  
  return response;
};


export function getSingleIngredient(name, amount, unit) {
  const response = fetch("https://spoonacular-recipe-food-nutrition-v1.p.rapidapi.com/recipes/parseIngredients?ingredientList=" + name + "&serving=" + amount + "&unit=" + unit, {
    method: 'POST',
    headers: new Headers({
      'X-RapidAPI-Key': '55d98e3beemshee3584b0a12de42p1cd3c6jsn38172b345589',
      'Content-Type': 'application/x-www-form-urlencoded'
    }),
  }).then(response => response.json()).catch((error) => console.log(error))

  return response;
}